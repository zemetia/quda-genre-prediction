from django.conf import settings
from django.core.files.storage import default_storage
from django.shortcuts import render
from django.http import HttpResponse
# from transformers import TFAutoModel, TFBertTokenizer, TFBertModel
import numpy as np
import pandas as pd
import tensorflow as tf
from . import controller
import tensorflow_text as text
# tf.contrib.resampler

#GLOBAL VARIABLE
genre_summary = ['Action', 'Adventure', 'Comedy', 'Drama', 'Ecchi', 'Fantasy', 'Hentai', 'Horror', 'Mystery', 'Romance', 'Sci-Fi', 'Slice of Life', 'Sports' ,'Supernatural']
model = tf.saved_model.load('fullmodel')

def get_genres(data, tolerance=0.3):
    result = []
    for i in range(len(genre_summary)):
        if data[i] > tolerance: result.append(genre_summary[i])
    return result

def predict(synopsis: str) -> list:
    result = model([synopsis])
    return get_genres(result[0])

def index(request) -> HttpResponse:
    if request.method == "POST":
        synopsis = request.POST.get('synopsis', '')
        result = predict(synopsis)
        
        return controller.returnJSON({
            "synopsis": synopsis,
            "genres": result
            })
    
    elif request.method == "GET": 
        return controller.redirect('/')