# quda-genre-prediction

Generate Python Virtual Environment
```bat
python -m venv venv
```

open the virtual env command prompt
```cmd
venv\Scripts\activation
```

install required modules
```py
pip install -r requirement.txt
```

generate the migration
```py
python manage.py migrate
```

run the server
```py
python manage.py runserver
```